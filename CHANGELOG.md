# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.5.1

- patch: Internal maintenance: fix suffixes not supported with latest pip packaging.

## 2.5.0

- minor: Bump cryptography package version to fix vulnerabilities.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 2.4.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.4.0

- minor: Implement feature to automatically resolve files content-type.
- patch: Internal maintenance: bump packages.

## 2.3.0

- minor: Implement feature to upload directory files without directory itself.

## 2.2.1

- patch: Refactor logs output.

## 2.2.0

- minor: Bump cryptography package version to fix vulnerabilities.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 2.1.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 2.0.0

- major: Implement logic and tests based on Python code. Breaking change! You have to update your pipe variables.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 1.1.0

- minor: Update azure-cli image to version 2.48.1.
- patch: Internal maintenance: update bitbucket-pipes-toolkit-bash.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update default image, pipe versions in bitbucket-pipelines.yml.

## 1.0.2

- patch: Documentation update: deploying with $web param.

## 1.0.1

- patch: Internal maintenance: make pipe structure consistent to others.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-storage-deploy for future maintenance purposes.

## 0.5.2

- patch: Update icon.

## 0.5.1

- patch: Standardising README and pipes.yml.

## 0.5.0

- minor: Switch naming conventions from task to pipes.

## 0.4.2

- patch: Move details to summary.

## 0.4.1

- patch: Restructure README.md to match user flow.

## 0.4.0

- minor: Added DEBUG option.

## 0.3.0

- minor: Fixed the version number in the pipe definition yaml and documentation.

## 0.2.0

- minor: Automatically add the --recursive option to the azcopy command if the source is a local directory.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Azure Storage deploy pipe.
