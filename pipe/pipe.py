import mimetypes
import os
import yaml
from azure.storage.blob import BlobServiceClient, ContentSettings

from bitbucket_pipes_toolkit import Pipe


schema = {
    'SOURCE': {'type': 'string', 'required': True},
    'DESTINATION_STORAGE_ACCOUNT_NAME': {'type': 'string', 'required': True},
    'DESTINATION_SAS_TOKEN': {'type': 'string', 'required': True},
    'DESTINATION_CONTAINER_NAME': {'type': 'string', 'required': True},
    'DESTINATION_BLOB_RELATIVE_PATH': {'type': 'list', 'required': False, 'default': ''},
    'OVERWRITE': {'type': 'list', 'required': False, 'default': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class AzureStorageDeploy(Pipe):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = os.getenv('SOURCE')
        self.destination_account = os.getenv('DESTINATION_STORAGE_ACCOUNT_NAME')
        self.destination_sas_token = os.getenv('DESTINATION_SAS_TOKEN')
        self.destination_container = os.getenv('DESTINATION_CONTAINER_NAME')
        self.destination_path = os.getenv('DESTINATION_BLOB_RELATIVE_PATH')
        self.overwrite = os.getenv('OVERWRITE')

    def copy_file_from_local_to_azure_storage(self, client, source, destination_path):
        self.log_debug(f'Uploading {source}')
        content_type = mimetypes.guess_type(source)[0]
        content_settings = ContentSettings(content_type=content_type) if content_type else None
        try:
            with open(source, 'rb') as data:
                client.upload_blob(
                    name=destination_path,
                    data=data,
                    overwrite=self.overwrite,
                    content_settings=content_settings
                )
        except Exception as e:
            self.fail(f'Deployment failed. Reason: {e}')

    def copy_dir_from_local_to_azure_storage(self, client):
        self.log_debug(f'Uploading {self.source}')

        prefix = f'{self.destination_path}/' if self.destination_path else ''

        if self.source.endswith('/*'):
            # Copy without directory. Do not update prefix.
            self.source = self.source.removesuffix('/*')
        else:
            # Copy with directory. Update prefix.
            prefix += f'{os.path.basename(self.source)}/'

        for root, dirs, files in os.walk(self.source):
            for name in files:
                dir_part = os.path.relpath(root, self.source)
                dir_part = '' if dir_part == '.' else f'{dir_part}/'
                file_path = os.path.join(root, name)
                destination_path = f'{prefix}{dir_part}{name}'
                self.copy_file_from_local_to_azure_storage(client, file_path, destination_path)

    def run(self):
        self.log_info("Starting deployment to Azure storage...")

        destination_url = f"{self.destination_account}?{self.destination_sas_token}"
        blob_destination_service_client = BlobServiceClient(destination_url)
        client = blob_destination_service_client.get_container_client(self.destination_container)

        destination = f'{self.destination_account}/{self.destination_container}'
        if self.destination_path:
            destination += f'/{self.destination_path}'

        self.log_debug(f'Destination: {destination}')

        # For cases when uploading directory or files of directory.
        # Do not change source here since prefix should be defined inside.
        if os.path.isdir(self.source.removesuffix('/*')):
            self.copy_dir_from_local_to_azure_storage(client)
        else:
            destination_path = f'{self.destination_path}/{os.path.basename(self.source)}' if self.destination_path else os.path.basename(self.source)
            self.copy_file_from_local_to_azure_storage(client, self.source, destination_path)

        self.success('Deployment successful.')


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = AzureStorageDeploy(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
