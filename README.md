# Bitbucket Pipelines Pipe: Azure Storage Deploy

Pipe to deploy to [Microsoft Azure Storage][azure storage].
Copies files and directories to Azure Blob or File storage
using [Python Azure Blob SDK][Python Azure Blob SDK].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/azure-storage-deploy:2.5.1
  variables:
    SOURCE: '<string>'
    DESTINATION_STORAGE_ACCOUNT_NAME: '<string>'
    DESTINATION_SAS_TOKEN: '<string>'
    DESTINATION_CONTAINER_NAME: '<string>'
    # DESTINATION_BLOB_RELATIVE_PATH: '<string>' # Optional.
    # OVERWRITE: '<boolean>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                             | Usage                                                                                                                              |
|--------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| SOURCE (*)                           | The source of the files to copy. This will be the path to a file or directory on the local filesystem where the pipe is executing. |
| DESTINATION_STORAGE_ACCOUNT_NAME (*) | Storage account name. More info: [Blob Resource URI][Blob Resource URI].                                                           |
| DESTINATION_SAS_TOKEN (*)            | A [SAS(Shared Access Signature) token][SAS token] (without delimiter `?`) for authenticating against the destination .             |
| DESTINATION_CONTAINER_NAME (*)       | Name of a Storage container.                                                                                                       |
| DESTINATION_BLOB_RELATIVE_PATH       | Blob relative path to deploy files. Default: `''`.                                                                                 |
| OVERWRITE                            | Parameter to indicate whether an destination blob should be overwritten. Default: `false`.                                         |
| DEBUG                                | Turn on extra debug information. Default: `false`.                                                                                 |

_(*) = required variable._


## Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment of your data to the Azure storage.
* You have configured the [Azure storage account][Azure storage account] and environment.
* You have configured the [SAS(Shared Access Signature) token][SAS token] for your destination or source.


## Examples

### Basic example
Upload a file to Azure Storage `my-container` container:

```yaml
script:
  - pipe: atlassian/azure-storage-deploy:2.5.1
    variables:
      SOURCE: 'myfile.txt'
      DESTINATION_STORAGE_ACCOUNT_NAME: 'https://mystorageaccount.blob.core.windows.net'
      DESTINATION_SAS_TOKEN: $AZURE_STORAGE_SAS_TOKEN
      DESTINATION_CONTAINER_NAME: 'my-container'
```


Upload a file to Azure Storage `my-container` container custom relative path:

```yaml
script:
  - pipe: atlassian/azure-storage-deploy:2.5.1
    variables:
      SOURCE: 'myfile.txt'
      DESTINATION_STORAGE_ACCOUNT_NAME: 'https://mystorageaccount.blob.core.windows.net'
      DESTINATION_SAS_TOKEN: $AZURE_STORAGE_SAS_TOKEN
      DESTINATION_CONTAINER_NAME: 'my-container'
      DESTINATION_BLOB_RELATIVE_PATH: 'my-folder'
```

Upload a directory to Azure Storage `my-container` container:
```yaml
script:
  - pipe: atlassian/azure-storage-deploy:2.5.1
    variables:
      SOURCE: 'my-directory'
      DESTINATION_STORAGE_ACCOUNT_NAME: 'https://mystorageaccount.blob.core.windows.net'
      DESTINATION_SAS_TOKEN: $AZURE_STORAGE_SAS_TOKEN
      DESTINATION_CONTAINER_NAME: 'my-container'
```

Upload a content of a directory without directory itself to Azure Storage `my-container` container:
```yaml
script:
  - pipe: atlassian/azure-storage-deploy:2.5.1
    variables:
      SOURCE: 'my-directory/*'
      DESTINATION_STORAGE_ACCOUNT_NAME: 'https://mystorageaccount.blob.core.windows.net'
      DESTINATION_SAS_TOKEN: $AZURE_STORAGE_SAS_TOKEN
      DESTINATION_CONTAINER_NAME: 'my-container'
```

Upload a directory and allow to overwrite the conflicting files/blobs at the destination:
```yaml
script:
  - pipe: atlassian/azure-storage-deploy:2.5.1
    variables:
      SOURCE: 'my-directory'
      DESTINATION_STORAGE_ACCOUNT_NAME: 'https://mystorageaccount.blob.core.windows.net'
      DESTINATION_SAS_TOKEN: $AZURE_STORAGE_SAS_TOKEN
      DESTINATION_CONTAINER_NAME: 'my-container'
      OVERWRITE: 'true'
      DEBUG: 'true'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,azure,storage
[azure storage]: https://azure.microsoft.com/services/storage/
[Python Azure Blob SDK]: https://learn.microsoft.com/en-us/azure/storage/blobs/storage-blob-copy-url-python#copy-a-blob-from-a-source-within-azure
[Blob Resource URI]: https://docs.microsoft.com/en-us/rest/api/storageservices/naming-and-referencing-containers--blobs--and-metadata#resource-uri-syntax
[Azure storage account]: https://docs.microsoft.com/en-us/azure/storage/common/storage-quickstart-create-account
[SAS token]: https://docs.microsoft.com/en-us/azure/storage/common/storage-dotnet-shared-access-signature-part-1
