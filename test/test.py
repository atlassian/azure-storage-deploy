import datetime
import os
import random
import shutil
from http import HTTPStatus
from pathlib import Path

import requests
from azure.identity import DefaultAzureCredential
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.storage import StorageManagementClient
from azure.storage.blob import generate_container_sas, ContainerSasPermissions

from bitbucket_pipes_toolkit.test import PipeTestCase


class AzureStorageTestCase(PipeTestCase):

    maxDiff = None

    def create_sas(self, account_key):
        # Create a SAS token that's valid for 30 minutes, as an example
        expiry_time = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)

        sas_token = generate_container_sas(
            account_name=self.storage_account_name,
            account_key=account_key,
            container_name=self.container_name,
            permission=ContainerSasPermissions(read=True, write=True),
            expiry=expiry_time,
        )

        return sas_token

    def setUp(self):
        super().setUp()
        # generated
        self.random_number = random.randrange(0, 1000)

        # locals - generated
        self.azure_resource_group = f"bbci-pipes-test-infra-{self.random_number}"
        self.local_dir = f"tmp/azure-storage-{self.random_number}"
        self.storage_account_name = f"bbpipetest{self.random_number}"
        self.container_name = "test-container"

        # locals - fixed
        self.location = "CentralUS"
        self.sku = "Standard_LRS"

        Path(f"{self.local_dir}/subdir").mkdir(parents=True, exist_ok=True)
        self.deployment_filename = f"{self.local_dir}/subdir/deployment-{self.random_number}.txt"
        self.not_deployed_filename = f"{self.local_dir}/subdir/not_deployed-{self.random_number}.txt"

        with open(self.deployment_filename, 'w+') as deployment_file:
            deployment_file.write("Pipelines is awesome!")

        with open(self.not_deployed_filename, 'w+') as not_deployed_file:
            not_deployed_file.write("Not deployed")

        self.azure_credential = DefaultAzureCredential()

        # Obtain the management object for resources.
        self.resource_client = ResourceManagementClient(
            self.azure_credential,
            subscription_id=os.getenv("AZURE_SUBSCRIPTION_ID")
        )

        # Provision the resource group.
        self.resource_client.resource_groups.create_or_update(
            self.azure_resource_group,
            {"location": self.location}
        )

        # Obtain the management object for storage.
        self.storage_client = StorageManagementClient(
            self.azure_credential,
            subscription_id=os.getenv("AZURE_SUBSCRIPTION_ID")
        )

        # Create the storage.
        self.storage_client.storage_accounts.begin_create(
            self.azure_resource_group,
            self.storage_account_name,
            {
                "sku": {
                    "name": self.sku
                },
                "location": self.location,
            }
        ).result()

        # Create the container.
        self.blob_container = self.storage_client.blob_containers.create(
            self.azure_resource_group,
            self.storage_account_name,
            self.container_name,
            {}
        )

        # Create the sas token.
        storage_keys = self.storage_client.storage_accounts.list_keys(
            self.azure_resource_group,
            self.storage_account_name
        )
        storage_keys = {v.key_name: v.value for v in storage_keys.keys}

        self.destination_storage_account_name = f"https://{self.storage_account_name}.blob.core.windows.net"
        self.destination_sas_token = self.create_sas(storage_keys["key1"])

    def tearDown(self):
        shutil.rmtree(self.local_dir, ignore_errors=True)

        self.resource_client.resource_groups.begin_delete(self.azure_resource_group)

    def test_file_upload(self):
        result = self.run_container(environment={
            "SOURCE": f"{self.local_dir}/subdir/deployment-{self.random_number}.txt",
            "DESTINATION_STORAGE_ACCOUNT_NAME": self.destination_storage_account_name,
            "DESTINATION_SAS_TOKEN": self.destination_sas_token,
            "DESTINATION_CONTAINER_NAME": self.container_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Deployment successful.', result)

        response_present = requests.get(
            f"{self.destination_storage_account_name}/{self.container_name}/deployment-{self.random_number}.txt?{self.destination_sas_token}"
        ).text

        self.assertIn("Pipelines is awesome!", response_present)

        response_not_present = requests.get(
            f"{self.destination_storage_account_name}/{self.container_name}/not_deployed-{self.random_number}.txt?{self.destination_sas_token}"
        )

        self.assertEqual(response_not_present.status_code, HTTPStatus.NOT_FOUND)

    def test_directory_upload(self):
        result = self.run_container(environment={
            "SOURCE": f"{self.local_dir}/subdir",
            "DESTINATION_STORAGE_ACCOUNT_NAME": self.destination_storage_account_name,
            "DESTINATION_SAS_TOKEN": self.destination_sas_token,
            "DESTINATION_CONTAINER_NAME": self.container_name,
            "DESTINATION_BLOB_RELATIVE_PATH": "my-folder",

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Deployment successful.', result)

        response = requests.get(
            f"{self.destination_storage_account_name}/{self.container_name}/my-folder/subdir/deployment-{self.random_number}.txt?{self.destination_sas_token}"
        ).text

        self.assertIn("Pipelines is awesome!", response)

    def test_directory_files_upload(self):
        result = self.run_container(environment={
            "SOURCE": f"{self.local_dir}/subdir/*",
            "DESTINATION_STORAGE_ACCOUNT_NAME": self.destination_storage_account_name,
            "DESTINATION_SAS_TOKEN": self.destination_sas_token,
            "DESTINATION_CONTAINER_NAME": self.container_name,
            "DESTINATION_BLOB_RELATIVE_PATH": "my-folder",
            "DEBUG": True,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Deployment successful.', result)

        response = requests.get(
            f"{self.destination_storage_account_name}/{self.container_name}/my-folder/deployment-{self.random_number}.txt?{self.destination_sas_token}"
        ).text

        self.assertIn("Pipelines is awesome!", response)


class AzureStorageValidationTestCase(PipeTestCase):

    maxDiff = None

    def test_missing_variables(self):
        result = self.run_container(environment={
            "SOURCE": 'test.txt',
            "DESTINATION_STORAGE_ACCOUNT_NAME": 'test',
            "DESTINATION_CONTAINER_NAME": 'test',

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('DESTINATION_SAS_TOKEN:\n- required field', result)
